import React from 'react';
import axios from 'axios';

class TestRoute extends React.Component {
  state = { response: '' };

  componentDidMount = () => {
    axios
      .get('/testroute')
      .then(res => {
        this.setState({ response: res.data });
      })
      .catch(err => console.error(err));
  };

  render = () => {
    return (
      <div>
        <h2>Server response</h2>
        <div>{this.state.response}</div>
      </div>
    );
  };
}

export default TestRoute;
