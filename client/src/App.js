import React from 'react';
import { Route, Switch } from 'react-router-dom';
import NewComponent from './components/NewComponent/NewComponent';
import TestRoute from './components/TestRoute/TestRoute';

const App = () => {
  return (
    <div>
      <h1>Hi, i'm a React App :)-)</h1>
      <p>Ola</p>
      <Switch>
        <Route path="/new" component={NewComponent} />
        <Route path="/route" component={TestRoute} />
      </Switch>
    </div>
  );
};

export default App;
